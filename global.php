<?php

  function Matrix_SendMessage($room, $msg, $html_msg)
  {
    $homeserver = "https://matrix.XXXXXXXX.com";
    $room_server = "matrix.XXXXXXXX.com";
    $token = "XXXXXXXXXXXXXXX";

    $room_id = "XXXXXXXXXXXXXXXXXXXX";
    
    if ($room_id == "")
      return false;

    $url = "{$homeserver}/_matrix/client/r0/rooms/%21{$room_id}:{$room_server}/send/m.room.message?access_token={$token}";
    
    if (isset($html_msg) and $html_msg !== '')
      $data = array("msgtype" => "m.text", 
                    "body" => $msg, 
                    "format" => "org.matrix.custom.html",
                    "formatted_body" => $html_msg);
    else
      $data = array("msgtype" => "m.text", 
                    "body" => $msg);
    
    $data_string = json_encode($data);
    
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data_string)));
    $output = curl_exec($ch); 
    curl_close($ch);

    //echo $output;
    return true;
  }
?>