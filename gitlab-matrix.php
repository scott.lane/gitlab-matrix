<?php
  include_once('global.php');

  /*
  https://gitlab.com/scott.lane/gitlab-matrix
  
  Based off 
    https://github.com/kanboard/plugin-gitlab-webhook
  
  See Also...
    https://gitlab.com/gitlab-org/gitlab/-/issues/22996
    https://docs.gitlab.com/ce/user/project/integrations/webhooks.html#events
    https://docs.rocket.chat/guides/administrator-guides/integrations/gitlab

  */
  
  class WebhookHandler
  {
    public $room = '';
    
    public function parsePayload(array $payload)
    {
      if (empty($payload['object_kind']))
        return false;
      
      switch ($payload['object_kind'])
      {
        case 'issue':
          return $this->handleIssueEvent($payload);
        case 'note':
          return $this->handleCommentEvent($payload);
        case 'push':
          return $this->handlePushEvent($payload);
      }
      
      return false;
    }

    public function handlePushEvent(array $payload)
    {
      foreach ($payload['commits'] as $commit) {
        $this->handleCommit($commit);
      }

      return true;
    }

    public function handleCommit(array $commit)
    {
      $msg = "{$commit['author']['name']} made a commit\n{$commit['message']}";
      $html = "{$commit['author']['name']} made a <font color=#6dbc8d><a href=\"{$commit['url']}\">commit</a></font><br> {$commit['message']}";
      
      Matrix_SendMessage($this->room, $msg, $html);
      return true;
    }

    public function handleIssueEvent(array $payload)
    {
      if (!isset($payload['object_attributes']))
        return false;
        
      switch ($payload['object_attributes']['action']) {
        case 'open':
            return $this->handleIssueOpened($payload);
        case 'close':
            return $this->handleIssueClosed($payload);
        case 'reopen':
            return $this->handleIssueReopened($payload);
        case 'update':
            return $this->handleIssueUpdated($payload);
      }

      return false;
    }

    public function handleIssueOpened(array $payload)
    {
      $comment = str_replace("\n", '<br>', $payload['object_attributes']['description']);
      //$comment = preg_replace('#!\[image\]\((.*)\)#', '<a href="'.$payload['project']['web_url'].'$1">image</a>', $comment);
      $comment = preg_replace('#!\[image\]\((.*)\)#', '<a href="$1">image</a>', $comment);

      $labels = array();

      if (isset($payload['object_attributes']['labels']))
      {
        if (!empty($payload['object_attributes']['labels']))
        foreach ($payload['object_attributes']['labels'] as $label){
          $labels[] = $label['title'];
        }
      } 
      
      $msg = "{$payload['user']['name']} opened Issue #{$payload['object_attributes']['iid']} {$payload['object_attributes']['title']}<br>{$comment}";
      $html = "{$payload['user']['name']} <font color=#6dbc8d><b>opened</b></font> <a href=\"{$payload['object_attributes']['url']}\">Issue #{$payload['object_attributes']['iid']}</a> <i>{$payload['object_attributes']['title']}</i><br>{$comment}";
      if (count($labels) > 0)
        $html .= "<br>";
        foreach ($labels as $label){
          $html .= " [{$label}]";   
        }
          
      $html ."<br>{$comment}";
      
      Matrix_SendMessage($this->room, $msg, $html);   
      return true;
    }

    public function handleIssueReopened(array $payload)
    {
      $msg = "{$payload['user']['name']} reopened Issue {$payload['object_attributes']['iid']} {$payload['object_attributes']['title']}";
      $html = "{$payload['user']['name']} <font color=#6dbc8d><b>reopened</b></font> <a href=\"{$payload['object_attributes']['url']}\">Issue #{$payload['object_attributes']['iid']}</a> <i>{$payload['object_attributes']['title']}</i>";

      Matrix_SendMessage($this->room, $msg, $html);   
      return true;
    }

    public function handleIssueClosed(array $payload)
    {
      $msg = "{$payload['user']['name']} closed Issue {$payload['object_attributes']['iid']} {$payload['object_attributes']['title']}";
      $html = "{$payload['user']['name']} <font color=#6dbc8d><b>closed</b></font> <a href=\"{$payload['object_attributes']['url']}\">Issue #{$payload['object_attributes']['iid']}</a> <i>{$payload['object_attributes']['title']}</i>";

      Matrix_SendMessage($this->room, $msg, $html);   
      return true;
    }
    
    public function handleIssueUpdated(array $payload)
    {
      $msg = "{$payload['user']['name']} updated Issue {$payload['object_attributes']['iid']} {$payload['object_attributes']['title']}";
      $html = "{$payload['user']['name']} <font color=#6dbc8d><b>updated</b></font> <a href=\"{$payload['object_attributes']['url']}\">Issue #{$payload['object_attributes']['iid']}</a> <i>{$payload['object_attributes']['title']}</i>";
       
      if (isset($payload['changes']['labels']))
      {
        $previous = array();
        $current = array();
        $added = array();
        $removed = array();
      
        if (!empty($payload['changes']['labels']['previous']))
        foreach ($payload['changes']['labels']['previous'] as $label){
          $previous[] = $label['title'];
        }
        
        if (!empty($payload['changes']['labels']['current']))
        foreach ($payload['changes']['labels']['current'] as $label){
          $current[] = $label['title'];
        }
        
        foreach ($previous as $label)
          if (!in_array($label, $current))
            $removed[] = $label;

        foreach ($current as $label)
          if (!in_array($label, $previous))
            $added[] = $label;
          
        if (count($added) > 0)
        {
          $html .= "<br>Added: ";
          foreach ($added as $label)
            $html .= " [{$label}]";
        }

        if (count($removed) > 0)
        {
          $html .= "<br>Removed: ";
          foreach ($removed as $label)
            $html .= " [{$label}]";
        }
      }

      Matrix_SendMessage($this->room, $msg, $html);   
      return true;
    }    

    public function handleCommentEvent(array $payload)
    {
      if (! isset($payload['issue']))
        return false;

      // https://docs.gitlab.com/ce/user/project/integrations/webhooks.html#comment-on-issue

      $comment = str_replace("\n", '<br>', $payload['object_attributes']['note']);
      $comment = preg_replace('#!\[image\]\((.*)\)#', '<a href="'.$payload['project']['web_url'].'$1">image</a>', $comment);
           
      $msg = "{$payload['user']['name']} commented on issue #{$payload['issue']['iid']} {$payload['issue']['title']}\n{$comment}";
      $html = "{$payload['user']['name']} <font color=#6dbc8d><b>commented</b></font> on <a href=\"{$payload['object_attributes']['url']}\">Issue #{$payload['issue']['iid']}</a> <i>{$payload['issue']['title']}</i><br>{$comment}";

      Matrix_SendMessage($this->room, $msg, $html);   
      return true;
    }
  }

  $hook = new WebhookHandler();
  //$hook->room = 'Support';
  $hook->room = 'GitLab';
  $hook->parsePayload(json_decode(file_get_contents('php://input'), true) ?: array());
?>