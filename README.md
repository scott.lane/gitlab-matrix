# GitLab-Matrix
PHP Webhook Intergation for Gitlab to Matrix / Element.io

Fix for this issue
- [Gitlab-Matrix Webhook Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/22996)

See Also:
- [GitLab Webhooks](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html#events)
- [rocket.chat Intergations Guide](https://docs.rocket.chat/guides/administrator-guides/integrations/gitlab)

## Access Token
https://webapps.stackexchange.com/questions/131056/how-to-get-an-access-token-for-element-riot-matrix

Log in to the account you want to get the access token for. Click on the name in the top left corner, then "Settings".
Click the "Help & About" tab (left side of the dialog).
Scroll to the bottom and click on <click to reveal> part of Access Token.
Copy your access token to a safe place.
